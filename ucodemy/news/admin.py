from django.contrib import admin

# Register your models here.

from models import SignUp

@admin.register(SignUp)

class SignUpAdmin(admin.ModelAdmin):

	date_hierarchy = 'created_time'
	list_display = ('email', 'full_name', 'created_time')